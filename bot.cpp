#include <iostream>
#include <sstream>
#include <boost/asio.hpp>

boost::asio::streambuf b;

void print_buff() {
    std::istream is(&b);
    std::string line;
    std::getline(is, line);
    std::cout << line << std::endl;
}

void handle_input(const boost::system::error_code &error, std::size_t bytes_transferred) {
    if (!error) {
        print_buff();
    }
}

int main(int argc, char* argv[]) {
    std::cout << "Booting up..." << std::endl;

    boost::asio::io_service my_io_service;
    boost::asio::ip::tcp::resolver resolver(my_io_service);
    boost::asio::ip::tcp::resolver::query query("irc.rizon.net", "6667");
    //boost::asio::ip::tcp::resolver::iterator iter = resolver.resolve(query);

    boost::asio::ip::tcp::socket socket(my_io_service);
    my_io_service.run();
    boost::asio::connect(socket, resolver.resolve(query));

    std::cout << "Looks like we connected" << std::endl;
    boost::asio::read_until(socket, b, '\n');
    print_buff();
    boost::asio::read_until(socket, b, '\n');
    print_buff();
    boost::asio::read_until(socket, b, '\n');
    print_buff();
    boost::asio::read_until(socket, b, '\n');
    print_buff();

    boost::asio::async_read_until(socket, b, '\n', handle_input);

    std::stringstream ss;
    ss.str("PASS asdf");
    boost::asio::write(socket, boost::asio::buffer(ss.str()));
    ss.str("NICK Jobler");
    boost::asio::write(socket, boost::asio::buffer(ss.str()));
    ss.str("USER jobler tolmoon tolsun :Like a cobbler");
    boost::asio::write(socket, boost::asio::buffer(ss.str()));

    std::cout << "I think we are connected maybe" << std::endl;

    return 0;
}
