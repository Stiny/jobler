#include <iostream>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/function.hpp>
#include <queue>



class IrcConnection
{
    public:
        IrcConnection(const std::string& server, int port, boost::function<void (const std::string&)> onMessage, boost::asio::io_service& io_service): s(server), p(port), onM(onMessage), ios(io_service), socket(io_service)
    {
    }
        void connect() 
        {
            somethingHappened = false;
            //DNS stuff
            boost::asio::ip::tcp::resolver resolver(ios);
            boost::asio::ip::tcp::resolver::query query(s , "0");
            boost::asio::ip::tcp::resolver::iterator iter = resolver.resolve(query);
            boost::asio::ip::tcp::resolver::iterator end;

            boost::system::error_code error;

            while(iter != end)
            {
                boost::asio::ip::tcp::endpoint endpoint = iter->endpoint();
                endpoint.port(p);
                socket.close();
                socket.connect(endpoint, error);
                iter++;
            }
            if(error)
            {
                std::cout << "ERROR" << std::endl;
                std::cout << error << std::endl;
            }

            std::cout << "Connected to: " << socket.remote_endpoint().address().to_string() << std::endl; 

            //read from the socket until a space is found
            boost::asio::async_read_until(socket, currentData, ' ', boost::bind(&IrcConnection::onReceiveFinished, this, boost::asio::placeholders::error));
        }
        void disconnect() 
        {
            socket.close();
        }
        void send(int priority, const std::string& message) 
        {
        }
        bool somethingHappened;

    private:
        std::string s;
        int p;
        boost::function<void (const std::string&)> onM;
        boost::asio::io_service& ios;
        boost::asio::ip::tcp::socket socket;
        std::priority_queue<std::string> outQueue;
        boost::asio::streambuf currentData;


        void onReceiveFinished(const boost::system::error_code& error)
        {
            if(error)
            {
                disconnect();
                std::cout << "ERRORRRR" << std::endl;
                std::cout << error << std::endl;
            }

            std::cout << "STUFF IS OCCURING" << std::endl;
            somethingHappened = true;

            std::istream stream(&currentData);
            std::string data;
            std::getline(stream, data);

            boost::asio::async_read_until(socket, currentData, ' ', boost::bind(&IrcConnection::onReceiveFinished, this, boost::asio::placeholders::error));
            ios.dispatch(boost::bind(onM, data));
        }


};


void onMe(const std::string& x)
{
    std::cout << "MESSAGE" << std::endl;
    std::cout << x << std::endl;
}

int main()
{
    boost::asio::io_service ios;
    std::string server = "irc.rizon.net";
    int port = 6667;

    IrcConnection x(server, port, onMe, ios);

    x.connect();
    ios.run();

    while(!x.somethingHappened)
    {
        //
    }

    return 0;

}
